var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
var t4 = document.getElementById("t4");
var t5 = document.getElementById("t5");
var t6 = document.getElementById("t6");
var para = document.getElementById("para");
function calc() {
    var x1 = parseFloat(t1.value);
    var y1 = parseFloat(t2.value);
    var x2 = parseFloat(t3.value);
    var y2 = parseFloat(t4.value);
    var x3 = parseFloat(t5.value);
    var y3 = parseFloat(t6.value);
    var s1 = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
    var s2 = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2));
    var s3 = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
    if (s1 + s2 > s3 && s2 + s3 > s1 && s1 + s3 > s2) {
        var a = Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2), 2));
        var b = Math.sqrt(Math.pow((x3 - x1), 2) + Math.pow((y3 - y1), 2));
        var c = Math.sqrt(Math.pow((x2 - x3), 2) + Math.pow((y2 - y3), 2));
        var s = (a + b + c) / 2;
        var area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        para.value = area.toString();
    }
    else {
        alert("The given points do not form a triangle");
    }
}
//# sourceMappingURL=app.js.map