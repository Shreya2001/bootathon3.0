var t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
var t2:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
var t3:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
var t4:HTMLInputElement=<HTMLInputElement>document.getElementById("t4");
var t5:HTMLInputElement=<HTMLInputElement>document.getElementById("t5");
var t6:HTMLInputElement=<HTMLInputElement>document.getElementById("t6");
var para:HTMLInputElement=<HTMLInputElement>document.getElementById("para");

function  calc()
{
    var x1:number = parseFloat(t1.value);
    var y1:number = parseFloat(t2.value);
    var x2:number = parseFloat(t3.value);
    var y2:number = parseFloat(t4.value);
    var x3:number = parseFloat(t5.value);
    var y3:number = parseFloat(t6.value);
    var s1:number = Math.sqrt(Math.pow((x1-x2),2) + Math.pow((y1-y2),2));
    var s2:number = Math.sqrt(Math.pow((x1-x3),2) + Math.pow((y1-y3),2));
    var s3:number = Math.sqrt(Math.pow((x3-x2),2) + Math.pow((y3-y2),2));

    if(s1+s2>s3 && s2+s3>s1 && s1+s3>s2)
    {
        var a:number = Math.sqrt(Math.pow((x1-x2),2) + Math.pow((y1-y2),2));
        var b:number = Math.sqrt(Math.pow((x3-x1),2) + Math.pow((y3-y1),2));
        var c:number = Math.sqrt(Math.pow((x2-x3),2) + Math.pow((y2-y3),2));

        var s:number = (a+b+c)/2;

        var area:number = Math.sqrt(s * (s-a) * (s-b) * (s-c));

        para.value = area.toString();
    }
    else
    {
        alert("The given points do not form a triangle");
    }
}