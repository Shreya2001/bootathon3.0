var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function add() {
    var num1 = +t1.value;
    var num2 = +t2.value;
    if (isNaN(num1) || isNaN(num2)) {
        alert("Invalid Information!!");
    }
    else {
        var sum = num1 + num2;
        document.getElementById("para").innerHTML = "The sum of the two numbers is : " + sum;
    }
}
function sub() {
    var num1 = +t1.value;
    var num2 = +t2.value;
    if (isNaN(num1) || isNaN(num2)) {
        alert("Invalid Information!!");
    }
    else {
        var sum = num1 - num2;
        document.getElementById("para").innerHTML = "The differnce of the two numbers is : " + sum;
    }
}
function mul() {
    var num1 = +t1.value;
    var num2 = +t2.value;
    if (isNaN(num1) || isNaN(num2)) {
        alert("Invalid Information!!");
    }
    else {
        var sum = num1 * num2;
        document.getElementById("para").innerHTML = "The product of the two numbers is : " + sum;
    }
}
function div() {
    var num1 = +t1.value;
    var num2 = +t2.value;
    if (isNaN(num1) || isNaN(num2)) {
        alert("Invalid Information!!");
    }
    else {
        var sum = Math.floor(num1 / num2);
        var remainder = num1 % num2;
        document.getElementById("para").innerHTML = "The quotient is : " + sum + "<br/>" + "The remainder is : " + remainder;
    }
}
function sin() {
    var num3 = +t3.value;
    var ans;
    if (isNaN(num3)) {
        alert("Invalid Information!!");
    }
    else {
        ans = Math.sin(num3 * Math.PI / 180);
    }
    document.getElementById("para1").innerHTML = "The sine value of " + num3 + " is " + ans;
}
function cos() {
    var num3 = +t3.value;
    var ans;
    if (isNaN(num3)) {
        alert("Invalid Information!!");
    }
    else {
        ans = Math.cos(num3 * Math.PI / 180);
    }
    document.getElementById("para1").innerHTML = "The cosine value of " + num3 + " is " + ans;
}
function tan() {
    var num3 = +t3.value;
    var ans;
    if (isNaN(num3)) {
        alert("Invalid Information!!");
    }
    else {
        ans = Math.tan(num3 * Math.PI / 180);
    }
    document.getElementById("para1").innerHTML = "The tan value of " + num3 + " is " + ans;
}
function sqrt() {
    var num3 = +t3.value;
    var ans;
    if (isNaN(num3)) {
        alert("Invalid Information!!");
    }
    else {
        ans = Math.sqrt(num3);
    }
    document.getElementById("para1").innerHTML = "The square root of " + num3 + " is " + ans;
}
function pow() {
    var num3 = +t3.value;
    var ans;
    if (isNaN(num3)) {
        alert("Invalid Information!!");
    }
    else {
        var p = +prompt("power to which the number is to be raised?");
        var ans = Math.pow(num3, p);
    }
    document.getElementById("para1").innerHTML = num3 + " raised to " + p + " is equal to " + ans;
}
//# sourceMappingURL=cal.js.map