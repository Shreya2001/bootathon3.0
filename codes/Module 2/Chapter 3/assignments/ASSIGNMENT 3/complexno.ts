let t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");

function sep()
{
    var str: String = t1.value;
    var real:number;
    var img:number;

    var i:number = str.indexOf("+");

    if (i!=-1)
    {
        real = +str.substring(0,i);
        img = +str.substring(i+1,str.length-1);
        document.getElementById("para").innerHTML = "The real part is : " + real + "<br/><br/>";
        document.getElementById("para").innerHTML += "The imaginary part is : " + img;
    }
    else
    {
        document.getElementById("para").innerHTML = "The real part is : " + str;
    }
}