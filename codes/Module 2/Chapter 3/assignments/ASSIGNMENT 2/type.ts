var t1:HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
var t2:HTMLInputElement = <HTMLInputElement>document.getElementById("t2");
var t3:HTMLInputElement = <HTMLInputElement>document.getElementById("t3");

function chck()
{
    var s1:number = +t1.value;
    var s2:number = +t2.value;
    var s3:number = +t3.value;

    if(isNaN(s1) || isNaN(s2) || isNaN(s3))
    {
        alert("The given data is invalid");
    }
    else
    {
        if(s1+s2>s3 && s2+s3>s1 && s1+s3>s2)
        {
            if(s1==s2 && s3==s1)
            {
                document.getElementById("para").innerHTML = "It is an equilateral triangle !";
            }
            
            else if(((s1*s1)==((s2*s2)+(s3*s3)) ||((s1*s1)+(s3*s3))==(s2*s2) || ((s1*s1)+(s2*s2))==(s3*s3) ))
            {
                if(s1!=s2 && s2!=s3 && s1!=s3)
                {
                document.getElementById("para").innerHTML="The triangle is a scalene right angled triangle";
                }
                else
                {
                    document.getElementById("para").innerHTML="The triangle is an isosceles right angled triangle";
                }
            }

            else if ((s1==s2 && s1!=s3) || (s1==s3 && s1!=s2) || (s2==s3 && s1!=s2))
            {
                document.getElementById("para").innerHTML= "The triangle is a isosceles triangle";
            } 

             else if (s1!=s2 && s1!=s3 && s2!=s3)
            {
                document.getElementById("para").innerHTML= "The triangle is a scalene triangle";
            
            }
        }
        else
        {
            alert("The given sides do not form a triangle!");
        }
    }
}
